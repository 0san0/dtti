$(function() {
	$('#signup-form').submit(function() {
		var is_validated = $(this).parsley('validate')

		if (is_validated) {
			$('#submit-button').val('Processing');

			$.post('/', $('#signup-form').serialize(), function (response) {
				if (response.is_success) {
					$('#signup-form').hide();
					$('#success-message').html(response.message).show();

					trackConversion();
				}
			});
		}

		return false;
	});
});

function trackConversion() {

    // Twitter:
    twttr.conversion.trackPid('l5svs', { tw_sale_amount: 0, tw_order_quantity: 0 });

	// Facebook
	window.fb_param = {};
	fb_param.pixel_id = '6015047995571';
	fb_param.value = '0.00';
	fb_param.currency = 'USD';
	(function(){
		var fpw = document.createElement('script');
		fpw.async = true;
		fpw.src = '//connect.facebook.net/en_US/fp.js';
		var ref = document.getElementsByTagName('script')[0];
		ref.parentNode.insertBefore(fpw, ref);
	})();

	// Google
	window.google_trackConversion({
		google_conversion_id: 954913563,
		google_conversion_language: "en",
		google_conversion_format: "3",
		google_conversion_color: "ffffff",
		google_conversion_label: "tSEACPnTq1gQm6arxwM",
		google_remarketing_only: false
	});
}

Shadowbox.init();


//Select List
(function($){

  var $selected = $('#state-select .selected');

  $('#state-select ul li a').click(function(e){
    //e.preventDefault();

    var selected_text = $(this).text();
    $selected.text(selected_text);

    /*Page sorting script here*/

  });

})(jQuery);
