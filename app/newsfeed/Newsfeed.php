<?php

class Newsfeed {

	private $facebook_app_id;
	private $facebook_app_secret;
	private $facebook_handle;
	private $facebook_page_id;

	private $twitter_handle;

	private $twitter_consumer_key;
	private $twitter_consumer_secret;
	private $twitter_access_token;
	private $twitter_access_token_secret;

	//private $instagram_handle;
	//private $instagram_client_id;
	//private $instagram_client_secret;
	//private $instagram_access_token;

	// private $flickr_handle;
	// private $flickr_user_id;
	// private $flickr_key;
	// private $flickr_secret;

	//private $youtube_handle;
	//private $youtube_key;


	private $delete_cache = 0;


	function __construct() {

	   //Facebook
       $this->facebook_app_id = '1355462577812868';
       $this->facebook_app_secret = 'f098a1f191d3154a309ed26fe732cd23';
       $this->facebook_handle = 'DontTaxTheInternet';
       $this->facebook_page_id = '10152717782899772';

       //Twitter
       $this->twitter_handle = '@NoInternetTax';
       $this->twitter_consumer_key = 	'wI3gLN3PLr54p9LIcFYTqgCWa';
       $this->twitter_consumer_secret = '8OJOJpWr46zE4qTNh4jrXMCy1iKP2qdPya2QfRcLO5Muj7o7a3';
       $this->twitter_access_token = '1283734244-VPa1hrHpLSyPvGD4fUj27ehTIQnl3Mp6W3dNDz9';
       $this->twitter_access_token_secret = '1hili9VhaB0o0J3HX8EYfTl9dh5SpltggfoAl9F79WKWa';

       //Instagram
       //$this->instagram_handle = get_field('instagram_handle', 'option');
       //$this->instagram_user_id = get_field('instagram_user_id', 'option');
       //$this->instagram_client_id = get_field('instagram_client_id', 'option');
       //$this->instagram_client_secret = get_field('instagram_client_secret', 'option');
       //$this->instagram_access_token = get_field('instagram_access_token', 'option');

       // //Flickr
       // $this->flickr_handle = get_field('flickr_handle', 'option');
       // $this->flickr_user_id = get_field('flickr_user_id', 'option');
       // $this->flickr_key = get_field('flickr_key', 'option');
       // $this->flickr_secret = get_field('flickr_secret', 'option');

       //YouTube
       //$this->youtube_handle = get_field('youtube_handle', 'option');
       //$this->youtube_key = get_field('youtube_key', 'option');
       //$this->youtube_playlist_id = get_field('youtube_playlist_id', 'option');
       //$this->youtube_url = get_field('youtube_url', 'option');

       //Delete Cache
       $this->delete_cache = false;

   	}

	private function fetchFacebookFeed() {
		// Fetch app access token
		$url = 'https://graph.facebook.com/oauth/access_token?client_id='. $this->facebook_app_id .'&client_secret='. $this->facebook_app_secret .'&grant_type=client_credentials';
		$response = file_get_contents($url);
		parse_str($response, $accessTokenResponse);

		if (!isset($accessTokenResponse['access_token'])) {
			return array();
			//throw new Exception('Could not get Facebook app access token');
		}
		$accessToken = $accessTokenResponse['access_token'];

		// Fetch Facebook feed
		$url = 'https://graph.facebook.com/'.$this->facebook_handle.'/feed?access_token=' . urlencode($accessToken);
		$response = json_decode(@file_get_contents($url));


		// If there was any error, just return an empty array
		if ($response === NULL) {
			return array();
		}

		$url = 'https://graph.facebook.com/326171977508457/?fields=picture&access_token=' . urlencode($accessToken);
		$pageInfo = json_decode(@file_get_contents($url));
		$picture_url = $pageInfo->picture->data->url;

		// Instantiate results array
		$results = array();

		// Loop through each post
		$content = '';
		foreach ($response->data as $story) {


			if (!isset ($story->from) || $story->from->id == $this->facebook_page_id) {

				list(,$postId) = explode('_', $story->id);
				if (!isset($story->message) || strlen($story->message) <= 0) {
					continue;
				}
				$message = $story->message;
				if (strlen($story->message) > 350) {
					$pos = strpos($story->message, ' ', 350);

					if ($pos !== FALSE) {
						$message = substr($story->message, 0, $pos);
					}
				}
				if ($message==$content) {
					continue;
				}
				$content = $message;

				if (!isset($story->picture) || empty($story->picture)) :
					$picture = FALSE;
				else :
					$picture = $story->picture;
				endif;

				$url = 'https://graph.facebook.com/'.$story->id;
				$response = json_decode(@file_get_contents($url));


				if (!empty($response) && !empty($response->link)) {
					$linked_content = $response->link;
					$linked_content_name = $response->name;
					$linked_content_image = empty($response->picture) ? '' : $response->picture;
				} else {
					$linked_content = FALSE;
				}

				$results[strtotime($story->created_time) . '.' . uniqid(true)] = (object)array(
					'type' => 'facebook',
					'content' => $this->facebookify($message),
					'picture' => $picture,
					'link' => 'https://www.facebook.com/'.$this->facebook_handle.'/posts/' . $postId,
					'linked_content' => $linked_content,
					'linked_content_name' => $linked_content_name,
					'linked_content_image' => $linked_content_image,
					'author' => '/'.$this->facebook_handle,
					'image' => $picture_url,
					'date' => date('M d',strtotime($story->created_time)),
					'timestamp' => strtotime($story->created_time),
				);
			}
		}
		return array_slice($results, 0, 21);
	}


	private function fetchTwitterFeed() {

		$twitter = new Abraham\TwitterOAuth\TwitterOAuth (
			$this->twitter_consumer_key,
			$this->twitter_consumer_secret,
			$this->twitter_access_token,
			$this->twitter_access_token_secret
		);

		try {
			$tweets = $twitter->get("statuses/user_timeline",
				[
					"screen_name" 		=> $this->twitter_handle,
					"count"				=> 25,
					"exclude_replies" 	=> true,
					"include_rts"		=> false,
				]
			);


			if (count($tweets)<=1) {
				return array();
			}

			// Loop through each tweet
			foreach ($tweets as $tweet) {
					$results[strtotime($tweet->created_at) . '.' . uniqid(true)] = (object)array(
						'type' => 'twitter',
						'content' => $this->twitterify($tweet->text),
						'picture' => '',
						'link' => 'https://twitter.com/' . $tweet->user->screen_name . '/status/' . $tweet->id_str,
						'author' => '@'.$tweet->user->screen_name,
						'name' => $tweet->user->name,
						'date' => date('M d', strtotime($tweet->created_at)),
						'timestamp' => strtotime($tweet->created_at),
						'id' => $tweet->id_str,
						'image' => $tweet->user->profile_image_url,
					);
			}

		} catch (Exception $e) {
			return array();
		}
		$results = empty($results) ? [] : $results;
		return $results;
	}
/*
	private function fetchInstagramFeed() {
		try {
			// Build instagram API URL and fetch response
			$url = 'https://api.instagram.com/v1/users/' . $this->instagram_user_id . '/media/recent/?count=50&access_token=' . $this->instagram_access_token;

			$browser = new Buzz\Browser(new Buzz\Client\Curl());
			$response = $browser->get($url);

			$results = json_decode($response->getContent());

			// If there was any error, just return an empty array
			if ($results === NULL) {
				return array();
			}

			$results = $results->data;
			$images = [];
			foreach ($results as $result) {
				$images[$result->created_time . '.' . uniqid(true)] = (object)array(
					'type' => 'instagram',
					'picture' => '',
					'link' => $result->link,
					'author' => '@'.$this->instagram_handle,
					'name' => $this->instagram_handle,
					'date' => date('M d', $result->created_time),
					'image' => $result->images->standard_resolution->url,
					'text' => $result->caption->text,
				);

			}
			$images = empty($images) ? [] : $images;
			return $images;

		} catch (Exception $e) {
			echo '<!-- Request failed: ' . $e->getMessage() . ' -->';
		}

	}
	*/
/*
	// private function fetchFlickrFeed() {

	// 	$args= [
	// 		'api_key' => $this->flickr_key,
	//  		'user_id' => $this->flickr_handle,
	// 		'perPage' => 48,
	// 		'extras'  => 'date_taken,date_upload,owner_name,media,url_z,url_q',
	// 		'format'  => 'json',
	// 		'nojsoncallback' => 1,

	// 	];

	// 	$args = http_build_query($args);

	// 	$url = 'https://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&' . $args;
	// 	$response = json_decode(file_get_contents($url));

	// 	$photos = $response->photos->photo;
	// 	$results = [];
	// 	foreach ($photos as $photo) {
	// 		if ($photo->media != 'photo') {
	// 			continue;
	// 		}
	// 		$timestamp = strtotime($photo->dateupload);
	// 		$results[ $photo->dateupload . '.' . uniqid(true)] = (object)array(
	// 				'type' => 'flickr',
	// 				'link' => 'https://www.flickr.com/photos/' . $this->flickr_handle . '/' . $photo->id,
	// 				'author' => $photo->ownername,
	// 				'name' => $photo->ownername,
	// 				'date' => date('M d, Y', $photo->dateupload),
	// 				'image' => !empty($photo->url_z) ? $photo->url_z : $photo->url_q,
	// 				'square_image' => $photo->url_q,
	// 			);
	// 	}
	// 	$results = empty($results) ? [] : $results;
	// 	return $results;

	// }

	// private function fetchFlickrAlbums() {

	// 	$args= [
	// 		'api_key' => $this->flickr_key,
	//  		'user_id' => $this->flickr_user_id,
	// 		'per_page' => 500,
	// 		'primary_photo_extras'  => 'date_taken,date_upload,owner_name,media,url_z,url_q',
	// 		'format'  => 'json',
	// 		'nojsoncallback' => 1,

	// 	];

	// 	$args = http_build_query($args);

	// 	$url = 'https://api.flickr.com/services/rest/?method=flickr.photosets.getList&' . $args;
	// 	$response = json_decode(file_get_contents($url));
	// 	$albums = $response->photosets->photoset;
	// 	$results = [];
	// 	foreach ($albums as $album) {
	// 		$timestamp = strtotime($album->primary_photo_extras->dateupload);
	// 		$results[ $album->primary_photo_extras->dateupload . '.' . uniqid(true)] = (object)array(
	// 				'type' => 'flickr',
	// 				'link' => 'https://www.flickr.com/photos/' . $this->flickr_handle . '/albums/' .  $album->id,
	// 				'author' => $this->flickr_handle,
	// 				'name' => $album->primary_photo_extras->ownername,
	// 				'date' => date('M d, Y', $album->primary_photo_extras->dateupload),
	// 				'image' => !empty($album->primary_photo_extras->url_z) ? $album->primary_photo_extras->url_z : $album->primary_photo_extras->url_q,
	// 				'square_image' => $album->primary_photo_extras->url_q,
	// 				'title' => $album->title->_content,
	// 				'description' => $album->description->_content,
	// 			);
	// 	}
	// 	$results = empty($results) ? [] : $results;
	// 	return $results;

	// }
*/
/*
	private function fetchNews() {
		// Instantiate results array
		$results = array();

		// The Query
		$args = ['posts_per_page' => 21, 'orderby' => 'date', 'order' => 'DESC', 'post_type' => 'news'];
		$posts = ExtendedTimber::get_posts($args,'ExtendedTimberPost');

		foreach ($posts as $post) {

			$results[strtotime($post->post_date) . '.' . uniqid(true)] = (object)array(
				'type' => 'news',
				'title' => $post->title_excerpt(100),
				'content' => $post->excerpt(400),
				'picture' => $post->featured_image(),
				'link' => $post->permalink(),
				'date' => date('M d', strtotime($post->post_date)),
				'img' => $post->featured_image(),
				'subheading' => $post->subtitle,
			);

		}
		$results = empty($results) ? [] : $results;
		return $results;
	}
	*/
/*
	private function fetchYouTubeFeed() {

		//TO get playlist ID: https://www.googleapis.com/youtube/v3/channels?part=contentDetails&forUsername=<USERNAME>&key=<KEY>
		//LOOK for "UPDLOADS" key

		$url = 'https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails,snippet&maxResults=48&playlistId='. $this->youtube_playlist_id . '&key=' . $this->youtube_key;
		$result = json_decode(file_get_contents($url));
		$results = $result->items;
		foreach ($results as $item) {
			$timestamp = strtotime($item->snippet->publishedAt);
			$videos[ $timestamp. '.' . uniqid(true)] = (object)array(
					'type' => 'youtube',
					'link' => 'https://www.youtube.com/watch?v=' . $item->contentDetails->videoId,
					'author' => $item->snippet->channelTitle,
					'author_url' => $this->youtube_url,
					'name' => $item->snippet->title,
					'description' => $item->snippet->description,
					'date' => date('M d', $timestamp),
					'image' => $item->snippet->thumbnails->high->url,
					'id' => $item->contentDetails->videoId,
				);
		}
		$videos = empty($videos) ? [] : $videos;
		return $videos;
	}
*/
	// private function fetchSavis() {

	// 	$url = 'http://www.gop.gov/data/savis.php';
	// 	$results = json_decode(file_get_contents($url));
	// 	foreach ($results as $item) {
	// 		if (strpos($item->bill_number,'S')===0) {
	// 			continue;
	// 		}
	// 		$time = str_replace(' 00:00:00','',$item->full_date) . ' ' . $item->time_of_vote;
	// 		$timestamp = strtotime($time);
	// 		$yea = $item->aye_gop + $item->aye_dem + $item->aye_ind;
	// 		$nay = $item->no_gop + $item->no_dem + $item->no_ind;
	// 		$total = $yea + $nay;
	// 		$yea_percent = $yea/$total * 100;
	// 		$nay_percent = $nay/$total * 100;
	// 		$savis[ $timestamp. '.' . uniqid(true)] = (object)array(
	// 				'type' => 'savis',
	// 				'title' => $item->title,
	// 				'question' => $item->question,
	// 				'date' => date('l F j, Y g:m A', $timestamp),
	// 				'id' => $item->idsavis,
	// 				'result' => $item->result=='Passed' ? 'passed' : 'not-passed',
	// 				'bill' => $item->bill_number,
	// 				'yea' => $yea,
	// 				'nay' => $nay,
	// 				'yea_percent' => $yea_percent,
	// 				'nay_percent' => $nay_percent,
	// 			);
	// 	}
	// 	krsort($savis);
	// 	$first = true;
	// 	foreach ($savis as $key=>$value) {
	// 		$savis[$key]->header = $first ? get_field('votes_heading', 'option') : '';
	// 		$first=false;
	// 	}
	// 	$savis = empty($savis) ? [] : $savis;
	// 	return $savis;
	// }



	function twitterify($ret) {
		$ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
		$ret = preg_replace("/#(\w+)/", "<a href=\"http://twitter.com/search?q=%23\\1\" target=\"_blank\">#\\1</a>", $ret);
	return $ret;
	}

	private function facebookify($ret) {
		$ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
		return $ret;
	}


	private function fetchItems() {

		$newsFeed['facebook'] = array_slice($this->fetchFacebookFeed(),0,25);
		$newsFeed['twitter'] = array_slice($this->fetchTwitterFeed(),0,25);
		//$newsFeed['instagram'] = array_slice($this->fetchInstagramFeed(),0,25);
		// $newsFeed['flickr'] = array_slice($this->fetchFlickrFeed(),0,48);
		//$newsFeed['youtube'] = array_slice($this->fetchYouTubeFeed(),0,48);
		//$newsFeed['news'] = array_slice($this->fetchNews(),0,25);
		//$newsFeed['savis'] = array_slice($this->fetchSavis(),0,6);


		return $newsFeed;
	}

	public function getItems() {

		$m = new Memcached();
		$m->addServer('localhost', 11211);

		// If cache item is non-existant or has expired
		if ( $this->delete_cache || !$m->get('dtti_newsfeed') || empty($m->get('dtti_newsfeed')) ) {
			$items = $this->fetchItems();
			$m->set('dtti_newsfeed', $items, time() + 1200); // Cache for 20 minutes
			return $items;
		} else {
			echo '<!-- SOCIAL: CACHED -->';
			//return $m->get('judiciary_newsfeed'); // Cache hit, return feed from cache
			$items = $this->fetchItems();
			$m->set('dtti_newsfeed', $items, time() + 1200); // Cache for 20 minutes
			return $items;
		}

	}

}
