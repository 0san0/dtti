<?php

class CreateController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth');
	}


	/**
	* Creates FAQ Page with empty form
	**/
	public function getFaq() {

		$isFAQ = true;

		$faqs_initial = DB::table('faq')
												->where('id', '>=', 1)
												->orderBy('order', 'asc')
												->get();
		$faqs = array();

		foreach ($faqs_initial as $faq) {
			$f['id'] = $faq->id;
			$f['order'] = $faq->order;
			$f['question'] = $faq->question;
			$f['answer'] = $faq->answer;

			array_push($faqs, $f);
		}

		return View::make('pages.admin.create.FAQ')
								 ->with('isFAQ', $isFAQ)
								 ->with('faqs', $faqs);

	}

	/**
	* Adds in new FAQ
	**/
	public function postFaq() {

		$post = Input::all();

		if (empty($post)) {

			Session::flash('message', 'Unable to save new FAQ!');
			return Redirect::to('create/faq');

		} else {

			$faq = new FAQ;

			if (empty($post['question'])) {
				Session::flash('message', 'Unable to save new FAQ! Question or answer text not entered.');
				return Redirect::to('create/faq');
			} else {
				$faq->question = $post['question'];
			}

			if (empty($post['answer'])) {
				Session::flash('message', 'Unable to save new FAQ! Question or answer text not entered.');
				return Redirect::to('create/faq');
			} else {
				$faq->answer = $post['answer'];
			}

			$faq_order = DB::table('faq')
											 ->max('order');

			if (empty($faq_order)) {
				$faq->order = 0;
			} else {
				$faq->order = $faq_order++;
			}

			$faq->save();
			Session::flash('message', 'New FAQ created for the following questiom:' . $faq->question);
			return Redirect::to('create/faq');
		}

	}

	/**
	* Creates Scorecard Page with empty form
	**/
	public function getScorecard()
	{
		$scorecards_initial = DB::table('scorecard')
		                          ->where('id', '>=', 1)
		                          ->orderBy('order', 'asc')
		                          ->get();
		$scorecards = array();

		foreach ($scorecards_initial as $scorecard){
			$scorecards[] = array(
				'id' => $scorecard->id,
				'order' => $scorecard->order,
				'first_name' => $scorecard->first_name,
				'last_name' => $scorecard->last_name,
				'party' => $scorecard->party,
				'supports_tax' => $scorecard->supports_tax,
				'twitter' => $scorecard->twitter,
				'facebook' => $scorecard->facebook,
				'picture_url' => $scorecard->picture
				);
		}

		return View::make('pages.admin.create.scorecard')
		             ->with('scorecards', $scorecards);
	}


	/**
	* Creates New Scorecard
	**/
	public function postScorecard()
	{

		/**
		* $post = array(
			'_token' => form token,
			'picture' => file,
			'first_name' => string,
			'last_name' => string,
			'party' => string,
			'supports_tax' => string: y, n, or u (=unknown)
			'twitter' => string,
			'facebook' => string
		);
		**/
		$post = Input::all();

		if (empty($post)) {

			Session::flash('message', 'Unable to save new scorecard!');

		} else {

			$scorecard = new Scorecard;

			if ( !empty($post['first_name']) && is_string($post['first_name']) ){
				$scorecard->first_name = $post['first_name'];
			} else {
				Session::flash('message', 'At least a first name required to create a scorecard!');
				return Redirect::to('create/scorecard');
			}

			if ( !empty($post['last_name']) && is_string($post['last_name']) ) {
				$scorecard->last_name = $post['last_name'];
			} else {
				$scorecard->last_name = '';
			}

			if ( !empty($post['party']) && is_string($post['party']) ) {
				$scorecard->party = $post['party'];
			} else {
				$scorecard->party = '';
			}

			if ( !empty($post['supports_tax'])) {

				$scorecard->supports_tax = $post['supports_tax'];

			}

			if ( !empty($post['twitter']) && is_string($post['twitter']) ) {
				$scorecard->twitter = $post['twitter'];
			} else {
				$scorecard->twitter = '';
			}

			if ( !empty($post['facebook']) && is_string($post['facebook']) ) {
				$scorecard->facebook = $post['facebook'];
			} else {
				$scorecard->facebook = '';
			}

			if ( !empty($post['picture']) && $post['picture']->isValid() ) {

				$file_extension = '.' . $post['picture']->getClientOriginalExtension();

				$img_dir = public_path() . '/img' . '/' . 'scorecards';
				$file_name = ( (!empty($post['first_name'])) ? $post['first_name'] : $scorecard->first_name ) . ( (!empty($post['last_name'])) ? $post['last_name'] : $scorecard->last_name ) . time() . $file_extension;
				$file_name = preg_replace('/\s/', '_', $file_name);

				$post['picture']->move($img_dir, $file_name);

				$scorecard->picture = URL::to('/') . '/img' . '/scorecards' . '/' . $file_name;

			} else {
				$scorecard->picture = '';
			}

			$last_scorecard = Scorecard::max('order');

			if ( empty($last_scorecard)) {
				$scorecard->order = 0;
			} else {
				$scorecard->order = $last_scorecard++;
			}

			$scorecard->save();
			Session::flash('message', 'New scorecard created for ' . $scorecard->first_name . ' ' . $scorecard->last_name . '!');

		}

		return Redirect::to('create/scorecard');

	}

	public function getVideo()
	{
		return View::make('pages.admin.create.video');
	}

	public function postVideo()
	{
		$rules = array(
			'title' => array('required', 'max:255'),
			'image' => array('image', 'mimes:jpeg,png,jpg'),
			'description' => array('max:1000'),
			'youtube_url' => array('required', 'url', 'max:1000'),
			'order' => array('required', 'integer')
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::to('create/video')
				->withInput()
				->withErrors($validator);
		}

		$url = Input::get('youtube_url');
		$parsed_url = parse_url($url);
		parse_str(parse_url($url, PHP_URL_QUERY ), $url_pieces);

		if (!preg_match('#youtube#', $parsed_url['host']) || !isset($url_pieces['v']) || empty($url_pieces['v']))
		{
			return Redirect::to('create/video')
				->withInput()
				->with('message', 'That YouTube URL is not valid!');
		}

		$post = new Post;
		$post->post_type = 'VIDEO';
		$post->user_id = Auth::user()->id;
		$post->order = Input::get('order');
		$post->save();

		$PostTypeVideo = new PostTypeVideo;
		$PostTypeVideo->post_id = $post->id;
		$PostTypeVideo->title = Input::get('title');
		$PostTypeVideo->description = Input::get('description');
		$PostTypeVideo->youtube_id = $url_pieces['v'];

		if (Input::hasFile('image'))
		{
			$file = Input::file('image');
			$mime_type = $file->getMimeType();

			$ext = 'none';
			if ($mime_type == 'image/png')
				$ext = 'png';

			if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
				$ext = 'jpg';

			$file_name = md5(Str::random(20).time()).'.'.$ext;
			$file->move('uploads', $file_name);

			$PostTypeVideo->image_file_name = $file_name;
		}

		$PostTypeVideo->save();

		Session::flash('message', 'Video successfully saved!');
		return Redirect::to('create/video');
	}

	public function getQuote()
	{
		return View::make('pages.admin.create.quote');
	}

	public function postQuote()
	{
		$rules = array(
			'author' => array('required', 'max:255'),
			'text' => array('required', 'max:2000'),
			'order' => array('required', 'integer')
			);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::to('create/quote')
			->withInput()
			->withErrors($validator);
		}

		$post = new Post;
		$post->post_type = 'QUOTE';
		$post->user_id = Auth::user()->id;
		$post->order = Input::get('order');
		$post->save();

		$postTypeQuote = new PostTypeQuote;
		$postTypeQuote->post_id = $post->id;
		$postTypeQuote->author = Input::get('author');
		$postTypeQuote->text = Input::get('text');
		$postTypeQuote->save();

		Session::flash('message', 'Quote successfully created!');
		return Redirect::to('create/quote');
	}

	public function getState() {

		return View::make('pages.admin.create.state');
	}

	public function postState() {

		if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) &&
		     empty($_FILES) && (int) $_SERVER['CONTENT_LENGTH'] > 0)
		{
		  $displayMaxSize = ini_get('post_max_size');

		  switch(substr($displayMaxSize,-1))
		  {
		    case 'G':
		      $displayMaxSize = $displayMaxSize * 1024;
		    case 'M':
		      $displayMaxSize = $displayMaxSize * 1024;
		    case 'K':
		       $displayMaxSize = $displayMaxSize * 1024;
		  }

		  $error = 'Posted data is too large. '.
		           $_SERVER['CONTENT_LENGTH'].
		           ' bytes exceeds the maximum size of '.
		           $displayMaxSize.' bytes.  Please upload smaller files.';
		           return Redirect::to('create/state')
					->withInput()
					->with('message',$error);
		}

		$rules = array(
			'name' => array('required', 'max:255'),
			'headline' => array('required', 'max:255'),
			'subheadline' => array('required', 'max:255'),
			'url' => array('required', 'max:2000'),
			'image_file_name' => array('image', 'mimes:jpeg,png,jpg'),
			'pdf_file_name' => array('mimes:pdf'),
			'body' => array('required', 'max:30000'),
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{

			return Redirect::to('create/state')
			->withInput()
			->withErrors($validator);
		}

		$state = new State;


		$state->name = Input::get('name');
		$state->headline = Input::get('headline');
		$state->subheadline = Input::get('subheadline');
		$state->url = Input::get('url');
		$state->body = Input::get('body');
		$state->dropdown = Input::get('dropdown')=='on' ? true : false;

		if (Input::hasFile('image_file_name'))
		{
			$file = Input::file('image_file_name');
			$mime_type = $file->getMimeType();

			$ext = 'none';
			if ($mime_type == 'image/png')
				$ext = 'png';

			if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
				$ext = 'jpg';

			$file_name = md5(Str::random(20).time()).'.'.$ext;
			$file->move('uploads', $file_name);

			$state->image_file_name = $file_name;
		}

		if (Input::hasFile('pdf_file_name'))
		{
			$file = Input::file('pdf_file_name');
			$mime_type = $file->getMimeType();

			$ext = 'none';
			if ($mime_type == 'application/pdf' || $mime_type == 'application/x-pdf')
				$ext = 'pdf';


			$file_name = md5(Str::random(20).time()).'.'.$ext;
			$file->move('uploads', $file_name);

			$state->pdf_file_name = $file_name;
		}

		$state->save();
		$id = $state->id;

		Session::flash('message', 'State successfully saved!');
		return Redirect::to('updatestate/'.$id);
	}

	public function getArticle()
	{
		return View::make('pages.admin.create.article');
	}

	public function postArticle()
	{
		$rules = array(
			'title' => array('required', 'max:255'),
			'excerpt' => array('required', 'max:2000'),
			'image' => array('image', 'mimes:jpeg,png,jpg'),
			'source' => array('required', 'max:255', 'url'),
			'url' => array('required', 'max:255', 'url'),
			'order' => array('required', 'integer')
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::to('create/article')
			->withInput()
			->withErrors($validator);
		}

		$post = new Post;
		$post->post_type = 'ARTICLE';
		$post->user_id = Auth::user()->id;
		$post->order = Input::get('order');
		$post->save();

		$postTypeArticle = new PostTypeArticle;
		$postTypeArticle->post_id = $post->id;
		$postTypeArticle->title = Input::get('title');
		$postTypeArticle->excerpt = Input::get('excerpt');
		$postTypeArticle->url = Input::get('url');
		$postTypeArticle->source = Input::get('source');

		if (Input::hasFile('image'))
		{
			$file = Input::file('image');
			$mime_type = $file->getMimeType();

			$ext = 'none';
			if ($mime_type == 'image/png')
				$ext = 'png';

			if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
				$ext = 'jpg';

			$file_name = md5(Str::random(20).time()).'.'.$ext;
			$file->move('uploads', $file_name);

			$postTypeArticle->image_file_name = $file_name;
		}

		$postTypeArticle->save();

		Session::flash('message', 'Article successfully saved!');
		return Redirect::to('create/article');
	}

	public function getSubpage()
	{
		return View::make('pages.admin.create.subpage');
	}

	public function postSubpage()
	{
		$rules = array(
			'title' => array('required', 'max:255'),
			'source' => array('required', 'max:255', 'url'),
			'author' => array('required', 'max:255'),
			'date' => array('required', 'max:255'),
			'excerpt' => array('required', 'max:2000'),
			'content' => array('required'),
			'thumb_image' => array('image', 'mimes:jpeg,png,jpg'),
			'banner_image' => array('image', 'mimes:jpeg,png,jpg'),
			'order' => array('required', 'integer')
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			return Redirect::to('create/subpage')
				->withInput()
				->withErrors($validator);
		}

		$post = new Post;
		$post->post_type = 'SUBPAGE';
		$post->user_id = Auth::user()->id;
		$post->order = Input::get('order');
		$post->save();

		$postTypeSubpage = new PostTypeSubpage;
		$postTypeSubpage->post_id = $post->id;
		$postTypeSubpage->title = Input::get('title');
		$postTypeSubpage->slug = Str::slug(Input::get('title'));
		$postTypeSubpage->source = Input::get('source');
		$postTypeSubpage->author = Input::get('author');
		$postTypeSubpage->date = Input::get('date');
		$postTypeSubpage->excerpt = Input::get('excerpt');
		$postTypeSubpage->content = Input::get('content');

		if (Input::hasFile('thumb_image'))
		{
			$file = Input::file('thumb_image');
			$mime_type = $file->getMimeType();

			$ext = 'none';
			if ($mime_type == 'image/png')
				$ext = 'png';

			if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
				$ext = 'jpg';

			$file_name = md5(Str::random(20).time()).'.'.$ext;
			$file->move('uploads', $file_name);

			$postTypeSubpage->thumb_file_name = $file_name;
		}

		if (Input::hasFile('banner_image'))
		{
			$file = Input::file('banner_image');
			$mime_type = $file->getMimeType();

			$ext = 'none';
			if ($mime_type == 'image/png')
				$ext = 'png';

			if ($mime_type == 'image/jpeg' || $mime_type == 'image/pjpeg')
				$ext = 'jpg';

			$file_name = md5(Str::random(20).time()).'.'.$ext;
			$file->move('uploads', $file_name);

			$postTypeSubpage->banner_file_name = $file_name;
		}

		$postTypeSubpage->save();

		Session::flash('message', 'Subpage successfully saved!');
		return Redirect::to('create/subpage');
	}
}