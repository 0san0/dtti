<?php

class SignupsController extends BaseController {
	
	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	public function getIndex()
	{		
		$signupsCount = Signup::count();
		return View::make('pages.admin.signups', compact('signupsCount'));
	}

	public function getSync()
	{
		$signups = Signup::all();

		$updated_signups = array();
		foreach ($signups as $signup)
		{
			if (is_null($signup->lng) || is_null($signup->lat) || is_null($signup->city) || is_null($signup->state))
			{
				$request = file_get_contents('http://api.geonames.org/postalCodeSearchJSON?postalcode='.$signup->zip_code.'&country=us&maxRows=1&username=engagedc');
		
				$result = json_decode($request, TRUE);	
			
				//TODO: check if over limit

				//validates response && validates result
				if (!is_null($result) && isset($result['postalCodes']) && !empty($result['postalCodes']))
				{
					$update_info = Signup::find($signup->id);
					$update_info->lat = $result['postalCodes'][0]['lat'];
					$update_info->lng = $result['postalCodes'][0]['lng'];
					$update_info->city = $result['postalCodes'][0]['placeName'];
					$update_info->state = $result['postalCodes'][0]['adminCode1'];
					$update_info->save();

					$updated_signups[] = $update_info;
				}
			}
		}

		$count = count($updated_signups);

		if ($count != 0)
			$flash_message = 'Successfully updated '.$count.' record(s).';
		else
			$flash_message = 'All possible records are up to date.';

		Session::flash('message', $flash_message);
		return Redirect::to('signups');
	}

	public function getMap()
	{
		return View::make('pages.admin.map');
	}

	public function getHeat()
	{
		$signups = Signup::orderBy('created_at','DESC')->take(25000)->get();
		$min_date = $signups->last()->created_at->format('d/m/Y');
		
		return View::make('pages.admin.heat')->with('signups', $signups)->with('min_date',$min_date);
	}

	public function getVector()
	{
		$states = array( 
			'AK' => 'Alaska',  
			'AL' => 'Alabama',  
			'AR' => 'Arkansas',  
			'AZ' => 'Arizona',  
			'CA' => 'California',  
			'CO' => 'Colorado',  
			'CT' => 'Connecticut',  
			'DE' => 'Delaware',  
			'DC' => 'District of Columbia',  
			'FL' => 'Florida',  
			'GA' => 'Georgia', 
			'HI' => 'Hawaii',  
			'IA' => 'Iowa',  
			'ID' => 'Idaho',  
			'IL' => 'Illinois',  
			'IN' => 'Indiana',  
			'KS' => 'Kansas',  
			'KY' => 'Kentucky',  
			'LA' => 'Louisiana',  
			'MA' => 'Massachusetts',  
			'MD' => 'Maryland',  
			'ME' => 'Maine',  
			'MI' => 'Michigan',  
			'MN' => 'Minnesota',  
			'MS' => 'Mississippi',  
			'MO' => 'Missouri',  
			'MT' => 'Montana',  
			'NC' => 'North Carolina',  
			'ND' => 'North Dakota',  
			'NE' => 'Nebraska',  
			'NH' => 'New Hampshire',  
			'NJ' => 'New Jersey',  
			'NM' => 'New Mexico',  
			'NV' => 'Nevada',  
			'NY' => 'New York',  
			'OH' => 'Ohio',  
			'OK' => 'Oklahoma',  
			'OR' => 'Oregon',  
			'PA' => 'Pennsylvania',  
			'RI' => 'Rhode Island',  
			'SC' => 'South Carolina',  
			'SD' => 'South Dakota',  
			'TN' => 'Tennessee',  
			'TX' => 'Texas',  
			'UT' => 'Utah',  
			'VA' => 'Virginia',  
			'VT' => 'Vermont',  
			'WA' => 'Washington',  
			'WI' => 'Wisconsin',  
			'WV' => 'West Virginia',  
			'WY' => 'Wyoming'
		);

		//$signups = Signup::all();
		$signups = Signup::orderBy('created_at','DESC')->take(25000)->get();
		$min_date = $signups->last()->created_at->format('d/m/Y');

		$signups_by_state;
		foreach ($signups as $signup)
		{
			if (!empty($signup->state))
			{
				$signups_by_state[$signup->state][] = $signup; 
			}
		}

		$signups_totals;
		foreach ($signups_by_state as $key => $value)
		{
			$signups_totals['US-'.$key] = count($value);
		}

		

		return View::make('pages.admin.vector')->with('signups', $signups_totals)->with('min_date',$min_date);
	}

	public function postMap()
	{
		return Signup::all();
	}

	public function getDownload()
	{
		$filename = storage_path() . '/signups.csv';

		//new
		$csvFile = new Keboola\Csv\CsvFile($filename);

		$csvFile->writeRow(array(
			'id',
			'first_name',
			'last_name',
			'email',
			'zip_code',
			'city',
			'state',
			'lng',
			'lat',
			'source'
		));

		Signup::chunk(200, function ($signups) use ($csvFile) {
			foreach ($signups as $signup)
			{
				$csvFile->writeRow(array(
					$signup->id,
					$signup->first_name,
					$signup->last_name,
					$signup->email,
					$signup->zip_code,
					$signup->city,
					$signup->state,
					$signup->lng,
					$signup->lat,
					$signup->source
				));
			}
		});

		return Response::download($filename);
	}
}