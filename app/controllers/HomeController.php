<?php

class HomeController extends BaseController {

	public function getIndexOld()
	{
		$posts = Post::orderBy('order')->paginate(9);

		foreach ($posts as $post) {
			if ($post->post_type == 'SUBPAGE' || $post->post_type == 'ARTICLE')
			{
				if (isset($post->postType->source) && !empty($post->postType->source))
				{
					$url = parse_url($post->postType->source);
					$post->postType->host = $url['host'];
				}
			}

			if ($post->post_type == 'VIDEO')
			{
				if (!isset($post->postType->image_file_name) || empty($post->postType->image_file_name))
				{
					$post->postType->thumbnail = 'http://img.youtube.com/vi/'.$post->postType->youtube_id.'/hqdefault.jpg';
				}
				else
				{
					$post->postType->thumbnail = 'uploads/'.$post->postType->image_file_name;
				}
			}
		}

		return View::make('pages.index')
			->with('posts', $posts);
	}

	public function postIndexOld() {
		$first_name = Input::get('first_name');

		$rules = array(
			'first_name' => array('required', 'alpha', 'max:255'),
			'last_name' => array('required', 'alpha', 'max:255'),
			'email' => array('required', 'email', 'max:255'),
			'zip_code' => array('required', 'max:12')
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			if (Request::ajax())
				return Response::json($validator->messages());
			else
				return Redirect::to('/')->withInput()->withErrors($validator);
		}
		else
		{
			$signup = new Signup;

			$signup->first_name = Input::get('first_name');
			$signup->last_name = Input::get('last_name');
			$signup->email = Input::get('email');
			$signup->zip_code = Input::get('zip_code');
			$signup->source = Input::has('source') ? Input::get('source') : null;

			$request = @file_get_contents('http://api.geonames.org/postalCodeSearchJSON?postalcode='.$signup->zip_code.'&country=us&maxRows=1&username=engagedc');

			if ($request) {
				$result = json_decode($request, TRUE);

				if (!is_null($result) && isset($result['postalCodes']) && !empty($result['postalCodes']))
				{
					$signup->lat = $result['postalCodes'][0]['lat'];
					$signup->lng = $result['postalCodes'][0]['lng'];
					$signup->city = $result['postalCodes'][0]['placeName'];
					$signup->state = $result['postalCodes'][0]['adminCode1'];
				}
			}

			$signup->save();

			//$this->scheduleEmails($signup);

			if (Request::ajax())
			{
				return Response::json(array('is_success' => 1, 'message' => 'Thank you for your support! Use the "Tweet" and "Share" buttons at the top of the page to help spread the word.'));
			}
			else
			{
				return Redirect::to('/')
					->with('success_message', 'Thank you for your support! Use the "Tweet" and "Share" buttons at the top of the page to help spread the word.')
					->with('is_success', 1);
			}
		}
	}




	public function getIndex()
	{
		$current_page = 'home';
		$posts = Post::orderBy('order')->paginate(20);

		foreach ($posts as $post) {
			if ($post->post_type == 'SUBPAGE' || $post->post_type == 'ARTICLE')
			{
				if (isset($post->postType->source) && !empty($post->postType->source))
				{
					$url = parse_url($post->postType->source);
					$post->postType->host = $url['host'];
				}
			}

			if ($post->post_type == 'VIDEO')
			{
				if (!isset($post->postType->image_file_name) || empty($post->postType->image_file_name))
				{
					$post->postType->thumbnail = 'http://img.youtube.com/vi/'.$post->postType->youtube_id.'/hqdefault.jpg';
				}
				else
				{
					$post->postType->thumbnail = 'uploads/'.$post->postType->image_file_name;
				}
			}
		}

		return View::make('pages.index2')
			->with('posts', $posts)
			->with('current_page', $current_page);
	}

	public function postIndex() {
		$first_name = Input::get('first_name');

		$rules = array(
			'first_name' => array('required', 'alpha', 'max:255'),
			'last_name' => array('required', 'alpha', 'max:255'),
			'email' => array('required', 'email', 'max:255'),
			'zip_code' => array('required', 'max:12')
		);

		$messages = array(
    	'first_name.required' => 'First name is required.',
    	'last_name.required' => 'Last name is required.',
    	'email.required' => 'An email address is required.',
    	'zip_code.required' => 'A zip is required.'
		);

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
			if (Request::ajax())
				return Response::json($validator->messages());
			else
				return Redirect::to('/')->withInput()->withErrors($validator);
		}
		else
		{
			$signup = new Signup;

			$signup->first_name = Input::get('first_name');
			$signup->last_name = Input::get('last_name');
			$signup->email = Input::get('email');
			$signup->zip_code = Input::get('zip_code');
			$signup->source = Input::has('source') ? Input::get('source') : null;

			$request = @file_get_contents('http://api.geonames.org/postalCodeSearchJSON?postalcode='.$signup->zip_code.'&country=us&maxRows=1&username=engagedc');

			if ($request) {
				$result = json_decode($request, TRUE);

				if (!is_null($result) && isset($result['postalCodes']) && !empty($result['postalCodes']))
				{
					$signup->lat = $result['postalCodes'][0]['lat'];
					$signup->lng = $result['postalCodes'][0]['lng'];
					$signup->city = $result['postalCodes'][0]['placeName'];
					$signup->state = $result['postalCodes'][0]['adminCode1'];
				}
			}

			$signup->save();

			//$this->scheduleEmails($signup);

			if (Request::ajax())
			{
				return Response::json(array('is_success' => 1, 'message' => 'Thank you for your support! Use the "Tweet" and "Share" buttons at the top of the page to help spread the word.'));
			}
			else
			{
				return Redirect::to('/')
					->with('success_message', 'Thank you for your support! Use the "Tweet" and "Share" buttons at the top of the page to help spread the word.')
					->with('is_success', 1);
			}
		}
	}

























	private function scheduleEmails($signup) {
		$view = View::make('emails.signup.first', compact('signup'));
		$premailer = new ScottRobertson\Premailer\Request();
		$html = $premailer->convert($view->render())->downloadHtml();
		Mailgun::send('emails.signup.dummy', compact('html'), function($message) use ($signup)
		{
			$message->subject('Thank you, ' . $signup->first_name);
			$message->to($signup->email, $signup->name);
		});

		$view = View::make('emails.signup.second', compact('signup'));
		$premailer = new ScottRobertson\Premailer\Request();
		$html = $premailer->convert($view->render())->downloadHtml();
		//Mailgun::send('emails.signup.dummy', compact('html'), function ($message) use ($signup)
		Mailgun::later(array('hours' => 24), 'emails.signup.dummy', compact('html'), function($message) use ($signup)
		{
			$message->subject('have they heard?');
			$message->to($signup->email, $signup->name);
		});

		$view = View::make('emails.signup.third', compact('signup'));
		$premailer = new ScottRobertson\Premailer\Request();
		$html = $premailer->convert($view->render())->downloadHtml();
		//Mailgun::send('emails.signup.dummy', compact('html'), function ($message) use ($signup)
		Mailgun::later(array('hours' => ((24*3) - 1)), 'emails.signup.dummy', compact('html'), function($message) use ($signup)
		{
			$message->subject('sharing is caring');
			$message->to($signup->email, $signup->name);
		});
	}

}
