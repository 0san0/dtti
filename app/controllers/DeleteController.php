<?php

class DeleteController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	function deletePost($id = NULL)
	{
		$post = Post::find($id);

		if (!$post)
		{
			Session::flash('message', 'Could not delete. Post does not exist!');
			return Redirect::to('admin');
		}
		else
		{
			$post->postType->delete();
			$post->delete();

			Session::flash('message', 'Successfully Deleted Post!');
			return Redirect::to('admin');
		}
	}

	function deleteState($id = NULL)
	{
		$state = State::find($id);

		if (!$state)
		{
			Session::flash('message', 'Could not delete. Post does not exist!');
			return Redirect::to('admin');
		}
		else
		{
			$state->delete();

			Session::flash('message', 'Successfully Deleted Post!');
			return Redirect::to('/admin/states');
		}
	}

	function deleteScorecard($id = NULL)
	{
		$scorecard = Scorecard::find($id);

		if (!$scorecard) {
			Session::flash('message', 'Could not delete. Scorecard does not exist!');
			return Redirect::to('create/scorecard');
		} else {
			Session::flash('message', 'Successfully Deleted Scorecard for ' . $scorecard->first_name . ' ' . $scorecard->last_name . '!');
			$scorecard->delete();
			return Redirect::to('create/scorecard');
		}
	}

	function deleteFaq($id = NULL)
	{
		$faq = FAQ::find($id);

		if (empty($faq)) {
			Session::flash('message', 'Could not delete. FAQ does not exist!');
			return Redirect::to('create/faq');
		} else {
			Session::flash('message', 'Successfully Deleted Faq with question text:' . $faq->question);
			$faq->delete();
			return Redirect::to('create/faq');
		}
	}

}