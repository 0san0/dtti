<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	@include('slices.head')
	
</head>

<body class="states">

	<header>


		<div class="inner">
		<img class="header-img" src="/uploads/{{ $state->image_file_name }}" alt="">



<!-- 			<h1 id="logo">Tell Congress Don't Tax the Internet</h1>
			<p>For years, Americans have successfully fought attempts by big government advocates to
			 aggressively tax and regulate the Internet. The &quot;Marketplace Fairness Act&quot; seeks
			  to end that success by giving the federal government's blessing to an Internet tax collection
			   scheme that would allow states to harass and audit businesses beyond their borders. Add your
			    name below and tell your elected officials &quot;Don't tax the Internet!&quot;</p> -->
			<ul class="social-links">
				<li class="twitter-share">
					<a href="https://twitter.com/intent/tweet?text=Protect Internet commerce and American small businesses. Tell Congress “Don’t tax the Internet!”&url=http://DontTaxtheInter.net&hashtags=NoNetTax" class="twitter">Tweet</a>
				</li>
				<li class="facebook-share">
					<a class="facebook" onclick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=Protect+Internet+commerce+and+American+small+businesses.+Tell+Congress+%22Don%E2%80%99t+tax+the+Internet%21%22&amp;p[summary]=&amp;p[url]=http://DontTaxtheInter.net&amp;&amp;p[images][0]=http://engagedev.com/notaxnet/example/images/dtti-avatar-nolines.png','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)">Share</a>
				</li>
			</ul>

		</div>
		<section class="form-wrap">

			<div class="inner">
				<h2>Sign the Petition:</h2>
				<h4>Show your opposition to burdensome new taxes by adding your name below:</h4>

				
				@include('slices.form')
				
			</div>
		</section>

	</header>
	<div id="container">
		<div id="main" role="main">
			<h2>{{ $state->subheadline }}</h2>

			@yield('content')

		</div>

		<aside>
			<div class="sidebar-logos">
				<a href="http://www.ntu.org/"><img class="" src="/img/logo-ntu.png" alt=""></a>
				<a href="http://www.rstreet.org/"><img class="" src="/img/logo-rstreet.png" alt=""></a>
			</div>
			<h3>Other States</h3>
			<!-- <select>
			  <option value="nc">North Carolina</option>
			  <option value="sc">South Carolina</option>
			</select> -->

			<div id="state-select" class="btn-group select-topic">
       
        <ul class="dropdown-menu" role="menu">

        	@foreach ($list as $item)
        		
			    <li><a href="/{{ $item->url }}">{{ $item->name }}</a></li>
			@endforeach
        </ul>
         <button type="button" class="btn btn-default btn-select-text"><span class="selected">{{ $state->name }}</span></button>
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
      </div>


			<!-- <div id="jstwitter"></div> -->
		</aside>

	</div>
	<footer>
		<a href="/privacy-policy" class="privacy">Privacy Policy</a>
<!-- 		<ul>
			<li class="atr"><a href="http://www.atr.org/">http://www.atr.org/</a></li>
			<li class="campaign"><a href="http://www.campaignforliberty.org/">http://www.campaignforliberty.org/</a></li>
			<li class="cei"><a href="http://cei.org/">http://cei.org/</a></li>
			<li class="freedom"><a href="http://freedomandprosperity.org/">http://freedomandprosperity.org/</a></li>
			<li class="heartland"><a href="http://heartland.org/">http://heartland.org/</a></li>
			<li class="ipi"><a href="http://www.ipi.org/">http://www.ipi.org/</a></li>
			<li class="ntu"><a href="http://www.ntu.org/">http://www.ntu.org/</a></li>
			<li class="rstreet"><a href="http://www.rstreet.org/">http://www.rstreet.org/</a></li>
		</ul> -->
	</footer>
	<!-- include jQuery -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>

	<!-- Include the plug-in -->
	<script src="/js/jquery.wookmark.js"></script>
	<script src="//platform.twitter.com/widgets.js"></script>
	<script src="/js/jquery.twitter.js"></script>
	<script src="/js/jquery.parsley.js"></script>
	<script src="/js/shadowbox.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/scripts.js?v=2"></script>
	<!-- Once the page is loaded, initalize the plug-in. -->
	<script type="text/javascript">
	$(window).load(function() {

		// Prepare layout options.
		// var options = {
		// 	autoResize: true, // This will auto-update the layout when the browser window is resized.
		// 	container: $('#main'), // Optional, used for some extra CSS styling
		// 	offset: 20, // Optional, the distance between grid items
		// 	itemWidth: 220 // Optional, the width of a grid item
		// };
		
		// Define Google Web Font config
		WebFontConfig = {
			google: {
				families: [ 'Open+Sans:300italic,400italic,700italic,700,600,300' ]
			},
			active: function() {
				// Fonts are now loaded
				$('#tiles li').wookmark(options);
			}
		};
		
		// Asynchronously load webfont.js
		var wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);	
		
	});
	$(function () {
    // start jqtweet!
    JQTWEET.loadTweets();
});		
	</script>
	<div id="tweet-template">
		<div class="item">
			{PROFILEPIC} 
			<span class="name">{NAME}</span>
			<span class="user">@{HANDLE}</span>

			<div class="tweet-wrapper">
				<span class="text">{TEXT}</span>
				<span class="time">{AGO}</span>
				<a href="https://twitter.com/intent/tweet?in_reply_to={ID}" class="reply icon-reply-1">Reply</a>
				<a href="https://twitter.com/intent/retweet?tweet_id={ID}" class="retweet icon-retweet">Retweet</a>
				<a href="https://twitter.com/intent/favorite?tweet_id={ID}" class="favorite icon-star-1">Favorite</a>
			</div>
		</div>
	</div>

</body>
</html>