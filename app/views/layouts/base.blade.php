<!DOCTYPE html>
<html lang="en">
  <head>
    @include('slices.head2')
  </head>

  <body @if( isset($body_class) ) class="{{$body_class}}" @endif>

    <div class="page-wrapper">

    <div class="sidebar-bg"></div>

    <aside class="left-sidebar">
      <div class="sidebar-top">
        <a href="{{ URL::to('/'); }}"><img src="/img/logo.jpg" alt="Dont Tax the Internet Logo"></a>
      </div><!--/sidebar top-->
      <nav class="site-nav">
        <ul class="site-nav-links">
          <li class="nav-link">
            <a href="{{ URL::to('/'); }}" @if( $current_page == 'home') class="active" @endif>
              <span class="link-icon">
                <span class="fa fa-newspaper-o"></span>
                <span class="link-text">News</span>
              </span><!--/link icon-->
            </a>
          </li>
          <li class="nav-link">
            <a href="/social" @if( $current_page == 'social') class="active" @endif>
              <span class="link-icon">
                <span class="fa fa-comments"></span>
                <span class="link-text">Social</span>
              </span><!--/link icon-->
            </a>
          </li>
          <li class="nav-link">
            <a href="/scorecard" @if( $current_page == 'scorecard') class="active" @endif >
              <span class="link-icon">
                <span class="fa fa-th"></span>
                <span class="link-text">Scorecard</span>
              </span><!--/link icon-->
            </a>
          </li>
          <li class="nav-link">
            <a href="/frequently-asked-questions" @if( $current_page == 'faqs') class="active" @endif>
              <span class="link-icon">
                <span class="fa fa-question-circle"></span>
                <span class="link-text">FAQS</span>
              </span><!--/link icon-->
            </a>
          </li>
        </ul>
      </nav>

      <a class="privacy-policy" href="{{ URL::to('privacy-policy'); }}">Privacy Policy</a>
    </aside><!--/left-sidebar-->

    <div class="content-wrapper">

      <header class="page-header">

        <section class="social-links">
          <div class="social-links-wrapper">
            <ul>
              <li class="social-link"><a href="#" class="twitter-share" data-share-text="Tell your representative, Dont tax the net!"><i class="fa fa-twitter"></i></a></li>
              <li class="social-link"><a href="#" class="facebook-share" data-share-link="http://donttaxtheinter.net/" data-share-caption="We don't need an Internet tax."><i class="fa fa-facebook"></i></a></li>
            </ul>
          </div>
        </section>

        <section class="signup-form">
          <div class="container">
          <h2 class="signup-form-header">Tell Congress: Don't Tax The Internet!</h2>
          <p>Show your opposition to burdensome new taxes by adding your name below:</p>
          @include('slices.form2')
          </div><!--/container-->

        </section><!--/form-->
      </header><!--/page-header-->

      @yield('content')

      </div><!--/content wrapper-->

      </div><!--/page wrapper-->

    <footer class="page-footer">

      <ul class="sponsors">
        <li class="sponsor-li atr"><a href="http://www.atr.org/" target="_blank"><img class="footer-logo" src="{{ URL::to('/') . '/img/sponsor_logos/americans_for_task.png'; }}"></a></li>
        <li class="sponsor-li campaign"><a href="http://www.campaignforliberty.org/" target="_blank"><img class="footer-logo" src="{{ URL::to('/') . '/img/sponsor_logos/campaign_for_liberty.png'; }}"></a></li>
        <li class="sponsor-li cei"><a href="http://cei.org/" target="_blank"><img class="footer-logo" src="{{ URL::to('/') . '/img/sponsor_logos/CEI.png'; }}"></a></li>
        <li class="sponsor-li freedom"><a href="http://freedomandprosperity.org/" target="_blank"><img class="footer-logo" src="{{ URL::to('/') . '/img/sponsor_logos/Freedom_Prosperity.png'; }}"></a></li>
        <li class="sponsor-li heartland"><a href="http://heartland.org/" target="_blank"><img class="footer-logo" src="{{ URL::to('/') . '/img/sponsor_logos/Heartland_institute.png'; }}"></a></li>
        <li class="sponsor-li ipi"><a href="http://www.ipi.org/" target="_blank"><img class="footer-logo" src="{{ URL::to('/') . '/img/sponsor_logos/IPI.png'; }}"></a></li>
        <li class="sponsor-li ntu"><a href="http://www.ntu.org/" target="_blank"><img class="footer-logo" src="{{ URL::to('/') . '/img/sponsor_logos/NTU.png'; }}"></a></li>
        <li class="sponsor-li rstreet"><a href="http://www.rstreet.org/" target="_blank"><img class="footer-logo" src="{{ URL::to('/') . '/img/sponsor_logos/R_Street.png'; }}"></a></li>
      </ul>
    </footer>

    <script type="text/javascript" src="/js/public.min.js"></script>
  </body>
</html>