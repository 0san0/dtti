
@if (count($faqs) >= 1)

<table class="admin-faqs-table" id="admin-faqs-table tablesorter">
  <thead class="admin-faq-head">
    <th>Question</th>
    <th>Answer</th>
    <th></th>
    <th></th>
  </thead>
  <tbody class="admin-faq-body">

  @foreach ($faqs as $faq)

  <tr data-order="{{ $faq['order'] }}" data-id="{{ $faq['id'] }}">
    <td class="faq-question">{{ $faq['question'] }}</td>
    <td class="faq-answer">{{ $faq['answer'] }}</td>
    <td class="faq-edit"><a href="{{ URL::to('updatefaq', array( $faq['id'] )); }}">Edit</a></td>
    <td class="faq-delete"><a class="delete-entry" href="{{ URL::to('deletefaq', array( $faq['id'] )); }}" data-url="{{ URL::to('deletefaq', array( $faq['id'] )); }}" data-cardvalue="<?php echo(strip_tags($faq['question'])); ?>" data-type="FAQ">Delete</a></td>
  </tr>

  @endforeach

  </tbody>
</table>

<form class="admin-faq-save" action="{{ URL::to('reorder/faqs/'); }}" method="POST">
  <input id="faq-order" type="hidden" value="" name="faq_order">
  <input class="btn btn-primary save-faq-order" type="submit" value="Save">
</form>

@endif