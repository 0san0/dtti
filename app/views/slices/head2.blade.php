<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>@yield('title')</title>
<link rel="shortcut icon" type="image/png" href="/img/favicon.ico" />
<meta name="description" content="For years, Americans have successfully fought attempts by big government advocates to aggressively tax and regulate the Internet. The &quot;Marketplace Fairness Act&quot; seeks to end that success by giving the federal government's blessing to an Internet tax collection scheme that would allow states to harass and audit businesses beyond their borders. Add your name below and tell your elected officials &quot;Don't tax the Internet!&quot;">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href='https://fonts.googleapis.com/css?family=Exo+2:500,300italic|Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/css/public.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.min.js"></script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5MC746"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5MC746');</script>
<!-- End Google Tag Manager -->

<!-- Facebook OAauth -->
<meta name="twitter:card" content="summary" />
<meta property="og:site_name" content="Don't Tax the Internet" />
<meta property="og:type" content="website" />
<meta property="og:image" content="http://donttaxtheinter.net/img/dtti.fb.png" />
<meta property="og:locale" content="en_US" />
<meta property="og:title" content="Don't Tax the Internet" />
<meta property="og:description" content="For years, Americans have successfully fought attempts by big government advocates to aggressively tax and regulate the Internet. The &quot;Marketplace Fairness Act&quot; seeks to end that success by giving the federal government's blessing to an Internet tax collection scheme that would allow states to harass and audit businesses beyond their borders. Add your name below and tell your elected officials &quot;Don't tax the Internet!&quot;" />
<meta property="og:url" content="http://donttaxtheinter.net/" />

<meta property="twitter:image" content="http://donttaxtheinter.net/img/dtti.twitter.png" />

<script src="/js/fb_sdk.js"></script>
<script src="/js/socialshare.js"></script>