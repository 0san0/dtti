@extends('layouts.subpage')

@section('title')
	{{ $post->postType->title }} | Don't Tax the Internet!
@stop

@section('content')

		<a href="{{ $post->postType->source }}">
		<article>
		 @if (!empty($post->postType->banner_file_name))
		<div class="image-wrap">
			<img src="/uploads/{{ $post->postType->banner_file_name }}" alt="{{ $post->postType->title }}"/>
		</div>
		@endif

		<div class="inner">
		<h1>{{ $post->postType->title }}</h1>
		<ul class="meta-data">
			<li class="source"><h5>{{ $post->postType->source }}h5></li>
			<li class="author"><h5>{{ $post->postType->author }}</h5></li>
			<li class="date"><h5>{{ $post->postType->date }}</h5></li>
		</ul>
		<section>{{ $post->postType->content }}</section>


		</div>

		</article>
		</a>
		<div class="clear"></div>
@stop