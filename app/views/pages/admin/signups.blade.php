@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/admin" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
			<a href="/signups/sync" class="btn btn-primary btn-large"><i class="ficon-refresh ficon-spin"></i> Sync</a>
			<a href="/signups/map" class="btn btn-primary btn-large"><i class="ficon-map-marker"></i> Map</a>
			<a href="/signups/heat" class="btn btn-primary btn-large"><i class="ficon-fire"></i> Heat Map</a>
			<a href="/signups/vector" class="btn btn-primary btn-large"><i class="ficon-globe"></i> Density Map</a>
			<a href="/signups/download" class="btn btn-primary btn-large btn-export"><i class="ficon-save"></i> Export</a>
		</div>
	</div>
@stop

@section('content')

	<div class="row">
		<div class="span12">
			<h1 class="big-page-title" align="middle">Signups</h1>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<h2><strong>Total Signups: </strong>{{ number_format($signupsCount) }}</h2>
		</div>
	</div>

@stop