@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/signups" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
		</div>
	</div>
@stop


@section('content')
<div class="row">
	<div class="span12">
		<div id="map"></div>
	</div>
</div>


	

<script>
		

				var styles = [
				{
					stylers: [
					{ saturation: -85 }
					]
				},{
					featureType: 'road',
					elementType: 'geometry',
					stylers: [
					{ hue: "#002bff" },
					{ visibility: 'simplified' }
					]
				},{
					featureType: 'road',
					elementType: 'labels',
					stylers: [
					{ visibility: 'off' }
					]
				}
				];


				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 5,
					center: new google.maps.LatLng(39.909736, -102.084961),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});

				map.setOptions({styles: styles});

				var infowindow = new google.maps.InfoWindow();

				var marker, i;

				var approximate_location;

				$.ajax({
					url 		: '/signups/map',
					type 		: 'POST',
					dataType 	: 'json',
					success 	: function(data) {

						$.each(data, function(index, location) {

							if (location.lat == null) {
								return;
							}

							marker = new google.maps.Marker({
								position: new google.maps.LatLng(location.lat, location.lng),
								map: map
							});

							google.maps.event.addListener(marker, 'click', (function(marker, i) {
								return function() {

	var content = '';
	content += '<strong>ID: </strong> '+location.id+'</a>' ;
	content += '<br>';
	content += '<strong>Email: </strong> <a href="mailto:'+location.email+'">'+location.email+'</a>';
	content += '<br>';
	content += '<strong>Name: </strong>'+location.first_name+' '+location.last_name;
	content += '<br>';
	content += '<strong>Address: </strong>'+location.city+', '+location.state+', '+location.zip_code;
	content += '<br>';
	content += '<strong>Signed at: </strong>'+location.created_at;


									infowindow.setContent(content);
									infowindow.open(map, marker);
								}
							})(marker, i));
						});

					}
				});


						
		
			
		</script>

@stop
