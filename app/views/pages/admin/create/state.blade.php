@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/admin/states" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
		</div>
	</div>
@stop

@section('content')

	<div class="row">
		<div class="span12">
			<h1 class="big-page-title" align="middle">Create a State</h1>
		</div>
	</div>


	<div class="row">
		<div class="span6">

			@foreach ($errors->all('<div class="alert alert-error">:message</div>') as $error)
				{{ $error }}
			@endforeach

			{{ Form::open(array('url' => 'create/state', 'class' => 'create-form', 'id' => 'article-form', 'files'=>true )) }}

				<div class="control-group {{ $errors->first('name', 'error') }}">
					{{ Form::label('name', 'Name', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('name', Input::old('name'), array('id' => 'name', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('headline', 'error') }}">
					{{ Form::label('headline', 'Headline', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('headline', Input::old('headline'), array('id' => 'headline', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('subheadline', 'error') }}">
					{{ Form::label('subheadline', 'Sub Headline', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('subheadline', Input::old('subheadline'), array('id' => 'subheadline', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('url', 'error') }}">
					{{ Form::label('url', 'URL', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('url', Input::old('url'), array('id' => 'url', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('title', 'error') }}">
					{{ Form::label('body', 'Body', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::textarea('body', Input::old('body'), array('id' => 'body', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group">
					{{ Form::label('image_file_name','Image', array('class'=>'control-label'))}}
					<div class="controls">
						{{ Form::file('image_file_name') }}
					</div>
				</div>

				<div class="control-group">
					{{ Form::label('pdf_file_name','PDF', array('class'=>'control-label'))}}
					<div class="controls">
						{{ Form::file('pdf_file_name') }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('source', 'error') }}">
					{{ Form::label('dropdown', 'Show in Dropdown', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::checkbox('dropdown', Input::old('dropdown'), true) }}
					</div>
				</div>
				<input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
				<div class="control-group">
					<div class="controls">
						  <button type="submit" class="btn">Create</button>
					</div>
				</div>
			{{ Form::close() }}
			<script type="text/javascript">
			jQuery(function()
			{
			    jQuery('#body').redactor({ minHeight: 400  });
			});
			</script>


		</div>
		<div class="span6"> 
	
			
		</div>
	</div>

@stop