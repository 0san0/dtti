@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/admin" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
		</div>
	</div>
@stop

@section('content')

	<div class="row">
		<div class="span12">
			<h1 class="big-page-title">Create a Quote</h1>
		</div>
	</div>


	<div class="row">
		<div class="span6">
		
			@foreach ($errors->all('<div class="alert alert-error">:message</div>') as $error)
				{{ $error }}
			@endforeach

			{{ Form::open(array('url' => 'create/quote', 'class' => 'create-form', 'id' => 'quote-form')) }}
				<div class="control-group {{ $errors->first('author', 'error') }}">
					{{ Form::label('author', 'Author', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('author', Input::old('author'), array('id' => 'author', 'onkeyup' => 'mirror_author();')) }}
					</div>
				</div>
				<div class="control-group {{ $errors->first('text', 'error') }}">
					{{ Form::label('text', 'Quote Text', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::textarea('text', Input::old('text'), array('id' => 'quote_text', 'onkeyup' => 'mirror_text();')) }}
					</div>
				</div>
				<div class="control-group {{ $errors->first('order', 'error') }}">
					{{ Form::label('order', 'Order #', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('order', Input::old('order')) }}
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						  <button type="submit" class="btn">Create</button>
					</div>
				</div>
			{{ Form::close() }}
		


		</div>
		<div class="span6">
			<ul id="tiles">
				<li class="quote"><h5><span class="icon-quote"></span>Quote</h5>
					<blockquote id="example-text">Place your quote text here.</blockquote>
					<p>--<span id="example-author">John Does</span></p>
				</li>
			</ul>
			<script>
				function mirror_text(){ document.getElementById('example-text').innerHTML=document.getElementById('quote_text').value; }
				function mirror_author(){ document.getElementById('example-author').innerHTML=document.getElementById('author').value; }
				window.onload = function() { mirror_author(); };
				window.onload = function() { mirror_text(); };
			</script>
		</div>
	</div>

@stop