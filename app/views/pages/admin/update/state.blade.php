@extends('layouts.admin')

@section('back')
	<div class="row small-links">
		<div class="span12">
			<a href="/admin/states" class="btn btn-primary btn-large"><i class="ficon-arrow-left"></i> Go Back</a>
		</div>
	</div>
@stop

@section('content')

	<div class="row">
		<div class="span12">
			<h1 class="big-page-title" align="middle">Update state</h1>
		</div>
	</div>


	<div class="row">
		<div class="span6">

			@foreach ($errors->all('<div class="alert alert-error">:message</div>') as $error)
				{{ $error }}
			@endforeach

			{{ Form::open(array('url' => 'updatestate/'.$state->id, 'class' => 'create-form', 'id' => 'article-form', 'files'=>true )) }}

				<div class="control-group {{ $errors->first('name', 'error') }}">
					{{ Form::label('name', 'Name', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('name', Input::old('name', $state->name), array('id' => 'name', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('headline', 'error') }}">
					{{ Form::label('headline', 'Headline', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('headline', Input::old('headline', $state->headline), array('id' => 'headline', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('subheadline', 'error') }}">
					{{ Form::label('subheadline', 'Sub Headline', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('subheadline', Input::old('subheadline', $state->subheadline), array('id' => 'subheadline', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('url', 'error') }}">
					{{ Form::label('url', 'URL', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('url', Input::old('url', $state->url), array('id' => 'url', $state->url, 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group {{ $errors->first('body', 'error') }}">
					{{ Form::label('body', 'Body', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::textarea('body', Input::old('body', $state->body), array('id' => 'body', 'onkeyup' => 'mirror_title();')) }}
					</div>
				</div>

				<div class="control-group ">
					{{ Form::label('image_file_name','Image', array('class'=>'control-label'))}}
					<div class="controls">
						{{ Form::file('image_file_name') }}
					</div>
					Current Image: {{ strlen($state->image_file_name)>0 ? asset('uploads/'.$state->image_file_name) : 'NONE' }}
				</div>

				<div class="control-group">
					{{ Form::label('pdf_file_name','PDF', array('class'=>'control-label'))}}
					<div class="controls">
						{{ Form::file('pdf_file_name') }}
					</div>

					Current File: {{ strlen($state->pdf_file_name)>0 ? asset('uploads/'.$state->pdf_file_name) : 'NONE' }}
				</div>
		
				<div class="control-group {{ $errors->first('source', 'error') }}">
					{{ Form::label('dropdown', 'Show in Dropown', array('class' => 'control-label')) }}
					<div class="controls">
						*{{ $state->dropdown }}*
						@if ($state->dropdown == null || $state->dropdown==0 )
						    {{ Form::checkbox('dropdown', Input::old('dropdown'), false) }}
						@else
						     {{ Form::checkbox('dropdown', Input::old('dropdown'), true) }}
						@endif

					</div>
					<input type="hidden" name="MAX_FILE_SIZE" value="1048576" /> 
				</div>

				<div class="control-group">
					<div class="controls">
						  <button type="submit" class="btn">Update</button>
					</div>
				</div>
			{{ Form::close() }}
		
			<script type="text/javascript">
			jQuery(function()
			{
			    jQuery('#body').redactor({ minHeight: 400  });
			});
			</script>

		</div>
		<div class="span6"> 
			
		
		</div>
	</div>

@stop