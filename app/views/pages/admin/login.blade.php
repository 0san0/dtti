@extends('layouts.admin')

@section('content')

	<div class="row">
			<div class="span12">
				<h2>Login</h2>
				
				{{ $errors->first('email', '<div class="alert alert-error">:message</div>') }}
				{{ $errors->first('password', '<div class="alert alert-error">:message</div>') }}

				@if (Session::has('login_error') && Session::get('login_error') === TRUE)
					<div class="alert alert-error">The logins don't match!</div>
				@endif

				{{ Form::open(array('url' => 'login', 'class' => 'form-horizontal')) }}
					<div class="control-group {{ $errors->first('email', 'error') }}">
						{{ Form::label('email', 'Email', array('class' => 'control-label')) }}
				    	<div class="controls">
							{{ Form::text('email', Input::old('email')) }}
						</div>
					</div>
					<div class="control-group {{ $errors->first('password', 'error') }}">
						{{ Form::label('password', 'Password', array('class' => 'control-label')) }}
						<div class="controls">
							{{ Form::password('password') }}
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							  <button type="submit" class="btn">Sign in</button>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>

@stop