@extends('layouts.admin')

@section('content')

		<div class="dashboard-links">
			<a href="/create/quote" class="btn btn-primary btn-large"><i class="ficon-plus"></i> Add a Quote</a>
			<a href="/create/article" class="btn btn-primary btn-large"><i class="ficon-plus"></i> Add a Link</a>
			<a href="/create/subpage" class="btn btn-primary btn-large"><i class="ficon-plus"></i> Add a Subpage</a>
			<a href="/create/video" class="btn btn-primary btn-large"><i class="ficon-facetime-video"></i> Add a Video</a>
			<a href="/create/scorecard" class="btn btn-primary btn-large"><i class="ficon-group"></i> Add a Scorecard</a>
			<a href="/create/faq" class="btn btn-primary btn-large"><i class="ficon-group"></i> Add a FAQ</a>
			<a href="/signups" class="btn btn-primary btn-large"><i class="ficon-group"></i> View Signups</a>
			<a href="/admin/states" class="btn btn-primary btn-large"><i class="ficon-group"></i> States</a>
		</div>


	<div class="row">
		<div class="span12">
			<ul id="tiles">

				@foreach ($posts as $post)

					@if ($post->post_type == 'QUOTE')
						<li class="quote"><h5><span class="icon-quote"></span>Quote</h5>
							<blockquote>{{ $post->postType->text }}</blockquote>
							<p>&mdash;{{ $post->postType->author }}</p>
							<div class="edit-bar">
				     		   	<a href="/update/{{ $post->id }}"># {{ $post->order }}</a>
					        	<a href="/update/{{ $post->id }}">Edit</a>
					        	<a href="/delete/{{ $post->id }}">Delete</a>
					        </div>
						</li>
					@endif

					@if ($post->post_type == 'ARTICLE')
					<li class="news">
				        <h5><span class="icon-article-alt"></span>Link</h5>

				        @if (!empty($post->postType->image_file_name))
							<img src="/uploads/{{ $post->postType->image_file_name }}">
				        @endif

				        <h3>{{ $post->postType->title }}</h3>
				        <p><a href="{{ $post->postType->source }}">{{ $post->postType->host }} //</a> {{ $post->postType->excerpt }}</p>
				        <a href="{{ $post->postType->url }}" class="read-more">Read&raquo;</a>
				        <div class="edit-bar">
    					    <a href="/update/{{ $post->id }}"># {{ $post->order }}</a>
				        	<a href="/update/{{ $post->id }}">Edit</a>
				        	<a href="/delete/{{ $post->id }}">Delete</a>
				        </div>
			        </li>
					@endif

					@if ($post->post_type == 'SUBPAGE')
					<li class="news">
				        <h5><span class="icon-article-alt"></span>News</h5>

				        @if (!empty($post->postType->thumb_file_name))
							<img src="/uploads/{{ $post->postType->thumb_file_name }}">
				        @endif

				        <h3>{{ $post->postType->title }}</h3>
				        <p><a href="{{ $post->postType->source }}">{{ $post->postType->host }} //</a> {{ $post->postType->excerpt }}</p>
				        <a href="/news/{{ $post->postType->slug }}" class="read-more">Read&raquo;</a>
				        <div class="edit-bar">
				        	<a href="/update/{{ $post->id }}"># {{ $post->order }}</a>
				        	<a href="/update/{{ $post->id }}">Edit</a>
				        	<a href="/delete/{{ $post->id }}">Delete</a>
				        </div>
			        </li>
					@endif

					@if ($post->post_type == 'VIDEO')
					<li class="video">
				        <h5><span class="awesome-font"><i class="ficon-facetime-video"></i></span>Video</h5>


						<a rel="shadowbox" href="https://www.youtube.com/embed/{{ $post->postType->youtube_id }}?autoplay=1"><img src="{{ str_replace('http://','https://',$post->postType->thumbnail) }}"><div class="ficon-play"></div></a>

				        <h3>{{ $post->postType->title }}</h3>

				        @if (!empty($post->postType->description))
				        	<p>{{ $post->postType->description }}</p>
				        @endif

				        <div class="edit-bar">
			     		   	<a href="/update/{{ $post->id }}"># {{ $post->order }}</a>
				        	<a href="/update/{{ $post->id }}">Edit</a>
				        	<a href="/delete/{{ $post->id }}">Delete</a>
				        </div>

			        </li>
					@endif

				@endforeach

			</ul>
		</div>
	</div>

@stop