@extends('layouts.base')

@section('title')
  Scorecard | Don't Tax the Internet
@stop

@section('content')

  <main class="page-content">
    <div class="scorecard-description">
      <div class="left"><span class="fa fa-th"></span>
      </div>
      <div class="right">
        <h2 class="scorecard-description-header">Scorecard</h2>
        Find out where elected officials stand on the Internet sales tax through our scorecard.
       <div class="filter-buttons">VIEW: </div>
      </div>

    </div>

    <div class="grid">
      <div class="sizer"></div>

    @foreach ($officials as $official)

      <article class="official-profile official-profile-masonry" style="background-image:url({{ $official->picture }})" data-party="{{ $official->party; }}" data-stance="{{ $official->supports_tax }}" data-id="{{ $official->party; }}">

        @if( $official->supports_tax == 'y' )<div class="position"><span class="position-icon position-for"><i class="fa fa-times"></i></span>For the Tax</div>
        @elseif( $official->supports_tax == 'n' )<div class="position"><span class="position-icon position-against"><i class="fa fa-check"></i></span>Against the Tax</div>
        @elseif( $official->supports_tax == 'u' )<div class="position"><span class="position-icon position-unknown"><i class="fa fa-question"></i></span>No Declared Position</div>
        @endif

        <div class="profile-content">
          <div class="party {{ strtolower($official->party) }}">{{ $official->party }}</div>
          <h3 class="name">{{ $official->first_name }} {{ $official->last_name }}</h3>

          <ul class="about">
            <li><a href="http://twitter.com/intent/tweet?text={{ $official->twitter }}" target="_blank"><span class="profile-icon twitter"><i class="fa fa-twitter"></i></span> Tweet {{ $official->first_name }}</a></li>
            <li><a href="http://facebook.com/{{ $official->facebook }}"><span class="profile-icon facebook"><i class="fa fa-facebook"></i></span>Message {{ $official->first_name }}</a></li>
          </ul><!--/about-->

        </div><!--/content-->
      </article><!--/profile-->

    @endforeach

    </div><!--/grid -->

  </main><!--/page content-->

@stop