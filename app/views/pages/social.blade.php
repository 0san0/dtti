@extends('layouts.base')

@section('title')
  Social | Don't Tax the Internet
@stop

@section('content')

  <main class="page-content social-feed">

    <div class="feed-wrapper" data-masonry='{ "itemSelector": ".social-post", "columnWidth": ".twitter-tweet",  "gutter": 0, "percentPosition": true}'>

       @foreach ( $social as $post )

      <article class="social-post @if ($post->type == 'twitter') twitter-tweet @elseif ($post->type == 'facebook') facebook-post @endif" @if ( !empty($post->picture) ) style="background-image: url({{ $post->picture }})"@endif >
        <div class="date">{{ $post->date }}</div>
        <header class="user-info">
          @if ($post->type == 'twitter')
            <img class="avatar" src="{{ $post->image }}" alt="Dont Tax the Internet Logo">
            <div class="name"><a href="https://twitter.com/NoInternetTax" target="_blank"><i class="fa fa-twitter"></i> No Internet Tax</a></div>
            <div class="user-name"><a href="https://twitter.com/NoInternetTax" target="_blank">@NoInternetTax</a></div>
          @elseif ($post->type == 'facebook')
            <img class="avatar" src="{{ $post->image }}" alt="Dont Tax the Internet Logo">
            <div class="name"><a href="http://www.facebook.com{{ $post->author }}" target="_blank"><i class="fa fa-facebook-official"></i> Don't Tax the Internet</a></div>
          @endif

        </header>

        <p>{{ $post->content }}</p>
        @if (!empty($post->linked_content) )<p>Read the Full Article: <a href="{{ $post->linked_content }}" target="_blank">{{ $post->linked_content_name }}</a></p>@endif


      @if ($post->type == 'twitter')
        <ul class="twitter-actions">
          <li><a href="https://twitter.com/intent/tweet?tweet_id={{ $post->id }}" target="_blank"><i class="fa fa-reply"></i></a></li>
          <li><a href="https://twitter.com/intent/retweet?tweet_id={{ $post->id }}" target="_blank"><i class="fa fa-retweet"></i></a></li>
          <li><a href="https://twitter.com/intent/like?tweet_id={{ $post->id }}"><i class="fa fa-star"></i></a></li>
        </ul>
      @elseif ($post->type == 'facebook')
        <div class="fb-social-buttons">
          <div class="fb-like"
            data-href="{{ $post->link }}"
            data-layout="box_count"
            data-action="like"
            data-share="true"
            data-show-faces="false">
          </div>
        </div>
      @endif
      </article>

      @endforeach

    </div><!--/feed wrapper-->

    <div class="read-more-wrapper">
      <a class="read-more twitter-read-more" href="http://twitter.com/NoInternetTax" target="_blank">See More on Twitter</a>
      <a class="read-more facebook-read-more" href="https://facebook.com/DontTaxTheInternet" target="_blank">See More Posts on Facebook</a>
    </div>

  </main><!--/page content-->



















@stop