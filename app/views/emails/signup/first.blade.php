@extends('layouts.email')

@section('content')
	<p>{{{ $signup->first_name }}}&mdash;</p>

	<p>Thanks for taking a stand against an Internet sales tax and adding your name to the petition!</p>

	<p>Now, your elected representatives in Congress need to hear from you. Will you take 90 seconds to write a brief message to your Senators and Representative?</p>

	<h2><a href="http://www.ebaymainstreet.com/campaign/oppose-internet-sales-tax-lame-duck-legislation?utm_campaign=2014-internet-sales-tax&utm_source=partner-NoNetTax&utm_medium=referral">&gt;&gt; Click here to get started &lt;&lt;</a></h2>

	<p>Corporate special interests are spending millions of dollars to lobby Congress as they try to push through an Internet sales tax that will make it harder for small, local businesses to succeed in a world of big-box retailers.</p>

	<p><strong><u>That’s why it’s absolutely crucial that you take the next step and contact your elected officials.</u></strong></p>

	<p><strong>&mdash; Don't Tax the Internet</strong></p>
@stop