<?php

use Illuminate\Database\Migrations\Migration;

class AddingSubpageType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('post_type_subpage', function($table)
		{
		    $table->increments('id');
		    $table->integer('post_id');
			$table->string('title');
			$table->string('author');
			$table->string('date');
			$table->text('excerpt');
			$table->text('content');
			$table->string('thumb_file_name');
			$table->string('banner_file_name');
		});

		Schema::table('posts', function($table)
		{
		    $table->dropColumn('post_type');
		});

		Schema::table('posts', function($table)
		{
		    $table->enum('post_type', array('QUOTE', 'ARTICLE', 'SUBPAGE'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('post_type_subpage');

		Schema::table('posts', function($table)
		{
		    $table->dropColumn('post_type');
		});

		Schema::table('posts', function($table)
		{
			$table->enum('post_type', array('QUOTE', 'ARTICLE'));
		});
	}

}