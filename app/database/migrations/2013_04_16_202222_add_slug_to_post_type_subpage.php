<?php

use Illuminate\Database\Migrations\Migration;

class AddSlugToPostTypeSubpage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('post_type_subpage', function($table)
		{
			$table->string('slug');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('post_type_subpage', function($table)
		{
			$table->dropColumn('slug');
		});
	}

}