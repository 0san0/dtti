<?php

use Illuminate\Database\Migrations\Migration;

class CreateScorecard extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scorecard', function($table)
		{
				$table->increments('id');
				$table->integer('order');
		    $table->string('first_name');
		    $table->string('last_name');
		    $table->string('party');
		    $table->string('supports_tax');
		    $table->string('picture');
		    $table->string('twitter');
		    $table->string('facebook');
		    $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
    		$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('scorecard', function($table)
		{
			$table->drop('scorecard');
		});
	}

}