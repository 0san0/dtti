<?php

use Illuminate\Database\Migrations\Migration;

class AddingGeocodeCols extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('signups', function($table)
		{
			$table->decimal('lat', 11, 8)->after('zip_code')->nullable();
			$table->decimal('lng', 11, 8)->after('zip_code')->nullable();
			$table->string('state')->after('zip_code')->nullable();
			$table->string('city')->after('zip_code')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('signups', function($table)
		{
			$table->dropColumn('lat');
			$table->dropColumn('lng');
			$table->dropColumn('state');
			$table->dropColumn('city');
		});
	}

}