<?php

use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function($table)
		{
		    $table->increments('id');
		    $table->enum('post_type', array('QUOTE', 'ARTICLE'));
		    $table->integer('user_id');
		    $table->timestamps();
		});

		Schema::create('post_type_quote', function($table)
		{
		    $table->increments('id');
		    $table->integer('post_id');
			$table->string('author');
			$table->text('text');
		});

		Schema::create('post_type_article', function($table)
		{
		    $table->increments('id');
		    $table->integer('post_id');
		    $table->string('title');
			$table->text('excerpt');
			$table->string('url');
			$table->string('image_file_name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
		Schema::drop('post_type_quote');
		Schema::drop('post_type_article');
	}

}