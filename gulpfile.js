'use strict';

const DEV = false;

var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var uglify = require('gulp-uglify');


gulp.task('styles', function() {

  return gulp.src( './build/sass/public.scss' )
  .pipe( sass().on('error', sass.logError) )
  .pipe( autoprefixer(
    {
      browsers: ['last 5 versions', 'not ie < 9'],
      cascade: false
    }
  ))
  .pipe( minifyCss() )
  .pipe( rename({suffix: '.min'}) )
  .pipe( gulp.dest('./public/css') );

});


gulp.task('scripts', function() {

  return gulp.src('./build/scripts/public.js')
    .pipe( jshint().on('error', gutil.log) )
    .pipe( jshint.reporter(stylish).on('error', gutil.log) )
    .pipe( uglify().on('error', gutil.log) )
    .pipe( rename({suffix: '.min'}) )
    .pipe( gulp.dest('./public/js') );

});

gulp.task('admin-styles', function() {

  return gulp.src( './build/sass/admin.scss' )
  .pipe( sass().on('error', sass.logError) )
  .pipe( autoprefixer(
    {
      browsers: ['last 5 versions', 'not ie < 9'],
      cascade: false
    }
  ))
  .pipe( minifyCss() )
  .pipe( rename({suffix: '.min'}) )
  .pipe( gulp.dest('./public/css') );

});

gulp.task('admin-scripts', function() {

  return gulp.src('./build/scripts/admin.js')
    .pipe( jshint() )
    .pipe( jshint.reporter(stylish) )
    .pipe( uglify() )
    .pipe( rename({suffix: '.min'}) )
    .pipe( gulp.dest('./public/js') );

});


gulp.task('watch', function() {
  gulp.watch('./build/sass//**/*.scss', ['styles']);
  gulp.watch('./build/scripts//**/*.js', ['scripts']);
});

gulp.task('watch:admin', function() {
  gulp.watch('./build/sass/admin.scss', ['admin-styles']);
  gulp.watch('./build/scripts/admin.js', ['admin-scripts']);
});


gulp.task( "default", ["watch"] );












