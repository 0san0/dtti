(function($){
    'use strict';

    //Buttonify H3 Tags in FAQ page
    $('.faq').each(function(){
      var that = $(this);
      $(that).find('h3').click(function(){
        $(that).find('.faq-toggle').trigger("click");
      });
    });

    $('[data-toggle="faq"]').on('click', function(){
      var $button = $(this);
      var isExpanded = $button.attr('aria-expanded') === 'false' ? false : true;
      var target = $button.attr('aria-controls');
      var $target = $('#' + target);
      var height = $target.get(0).scrollHeight;

      if( isExpanded ){

        $button.attr('aria-expanded', 'false');

        $target.animate({
          height: 0
        }, 400, function() {

        });

      }else{

        $button.attr('aria-expanded', 'true');

        $target.animate({
          height: height
        }, 400, function() {
          $target.css('height', 'auto');

        });
      }
    });

    var reps = [];
    var repCount = 0;
    var dems = [];
    var demCount = 0;
    var other = [];
    var otherCount = 0;

    var supporters = [];
    var supportsCount = 0;
    var opposers = [];
    var opposersCount = 0;
    var unknown = [];
    var unknownCount = 0;
    $(".official-profile").each(function(){
      var party = $(this).data('party');
      if (party === 'Republican') {
        reps.push($(this));
        repCount++;
      } else if (party === 'Democrat' ) {
        dems.push($(this));
        demCount++;
      } else {
        other.push($(this));
        otherCount++;
      }

      var stance = $(this).data('stance');
      if (stance === 'y') {
        supporters.push($(this));
        supportsCount++;
      } else if (stance === 'n' ) {
        opposers.push($(this));
        opposersCount++;
      } else {
        unknown.push($(this));
        unknownCount++;
      }
    });

    initiateButtons();

    function initiateButtons() {

      if (repCount >= 1 ) {
        $('.filter-buttons').append('<button class="scorecard-filter rep-filter rep-shown" data-party="Republican">Rep</button>');
      }

      if (demCount >= 1) {
        $('.filter-buttons').append('<button class="scorecard-filter dem-filter dem-shown" data-party="Democrat">Dem</button>');
      }

      if (otherCount >= 1) {
        $('.filter-buttons').append('<button class="scorecard-filter other-filter other-shown" data-party="Other">Ind</button>');
      }

      if (supportsCount >= 1) {
        $('.filter-buttons').append('<button class="scorecard-filter supports-filter supporter-shown" data-stance="y"><i class="fa fa-times"></i></button>');
      }

      if (opposersCount >= 1) {
        $('.filter-buttons').append('<button class="scorecard-filter oppose-filter oppose-shown" data-stance="n"><i class="fa fa-check"></i></button>');
      }

      if (unknownCount >= 1) {
        $('.filter-buttons').append('<button class="scorecard-filter unknown-filter unknown-shown" data-stance="u"><i class="fa fa-question"></i></button>');
      }

      buttonify();

    }

    var $grid = $('.grid');

    $grid.masonry({
      itemSelector: '.official-profile-masonry',
      columnWidth: '.sizer'
    });

    $(window).resize(function() {
      $grid.masonry({
        itemSelector: '.official-profile-masonry',
        columnWidth: '.sizer'
      });
    });

    function buttonify() {
      $(".scorecard-filter").click(function(){

        if ( $(this).hasClass('shown') ) {

          $(".scorecard-filter").removeClass('hidden shown');

          $('.official-profile')
            .css("display", "block")
            .addClass("official-profile-masonry");

        } else {
          $(".scorecard-filter").addClass('hidden').removeClass('shown');
          $(this).removeClass('hidden').addClass('shown');

          var party = $(this).data('party');
          var stance = $(this).data('stance');

          if (party !== undefined) {

              $('.official-profile').each(function(){
                var officialParty = $(this).data('party');

                if ((officialParty !== 'Republican') && (officialParty !== 'Democrat')) {
                  officialParty = 'Other';
                }

                if (party === officialParty) {
                  $(this).css({'display': 'block'}).addClass('official-profile-masonry');
                } else {
                  $(this).css({'display': 'none'}).removeClass('official-profile-masonry');
                }

              });

          }

          if (stance !== undefined) {

             $('.official-profile').each(function(){
              var officialStance = $(this).data('stance');

              if (officialStance === stance) {
                $(this).css({'display': 'block'}).addClass('official-profile-masonry');
              } else {
                $(this).css({'display': 'none'}).removeClass('official-profile-masonry');
              }
             });
          }
        }

        $grid.masonry({
          itemSelector: '.official-profile-masonry',
          columnWidth: '.sizer'
        });
      });
    }

    // Track sign up data with FB pixel
    $('#signup-form').find('#submit-button').click(function(e){
      var $form  = $('#signup-form');

      var signupData = {
        firstName: $form.find('#first_name').val(),
        lastName: $form.find('#last_name').val(),
        email: $form.find('#email').val(),
        zipCode: $form.find('#zip_code').val()
      };

      fbq('track', 'Lead', signupData);

    });

})(jQuery);